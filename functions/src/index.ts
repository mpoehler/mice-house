'use strict';

import * as functions from 'firebase-functions';

import * as admin from 'firebase-admin';
admin.initializeApp(functions.config().firebase);

export const increaseMouseWheelCounter = functions.https.onRequest((request, response) => {    
    const increaseBy = Number(request.query.increaseBy);
    if (Number.isInteger(increaseBy)) {
        response.send("Counter increased");
        const date = new Date();
        const refName = '/mousewheel/'+date.getFullYear()+(date.getMonth()<10 ? '0' : '')+date.getMonth()+(date.getDate()>9 ? '' : '0')+date.getDate();
        return admin.database().ref(refName).transaction(function(current : number) {
            if (current == null) {
                return increaseBy;    
            }
            return current + increaseBy;                    
        });            
    } else {
        response.send("increasedBy is no number");
        console.error("increasedBy is no number");
        return 0;
    }
});
